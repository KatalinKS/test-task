<?php
return [
  'client_id' => getenv('VK_CLIENT_ID'),
  'client_secrete' => getenv('VK_CLIENT_SECRET'),
  'url_success_auth' => getenv('APP_URL').getenv('URL_SUCCESS_AUTH'),
];
