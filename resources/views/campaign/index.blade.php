<div style="width: 1000px; margin-left: auto; margin-right: auto">

@include('blocks.user')

    <div style="margin-bottom: 25px;">
        <a href="{{route('adv-cabinet')}}">Кабинеты </a> \
        <a href="{{route('get-adv-cabinet', $cabinet->account_id)}}">{{$cabinet->account_name}}</a> \
        <a href="{{route('get-adv-company', [$cabinet->account_id, $campaign->id])}}">{{$campaign->name}}</a>
    </div>
    <div>
        <h2>Рекламная компания {{$campaign->name}}</h2>
    </div>
    @foreach ($advs as $adv)
        <h3>{{$adv->name}}</h3>
        <div style="border-bottom: 1px solid rgba(223,223,223,1); padding: 10px 5px">
            <table>
                <tr>
                    <td style="width: 250px">Название компании:</td>
                    <td>{{$campaign->name}}</td>
                </tr>
                <tr>
                    <td>Тематики:</td>
                    <td>{{$adv->categories->implode('name', ', ')? $adv->categories->implode('name', ', ') : 'не задано'}}</td>
                </tr>
                <tr>
                    <td>Дневной лимит:</td>
                    <td>{{$adv->day_limit}}</td>
                </tr>
                <tr>
                    <td>Лимит обьявления:</td>
                    <td>{{$adv->all_limit}}</td>
                </tr>
                <tr>
                    <td>Цена за 1000 показов:</td>
                    <td>{{$adv->cpm/100}} ₽</td>
                </tr>
                <tr>
                    <td>Статус:</td>
                    <td>
                        @switch($adv->status)
                            @case(0)
                            объявление остановлено
                            @break

                            @case(1)
                            объявление запущено
                            @break

                            @case(1)
                            объявление удалено
                            @break
                        @endswitch
                    </td>
                </tr>
                <tr>
                    <td>Дата запуска:</td>
                    <td>{{$adv->start_time == 0 ? 'не задано' : $adv->start_time}}</td>
                </tr>
                <tr>
                    <td>Дата остановки:</td>
                    <td>{{$adv->stop_time == 0 ? 'не задано' : $adv->stop_time}}</td>
                </tr>
                <tr>
                    <td>Рекламные площадки:</td>
                    <td>
                        @switch($adv->ad_format)
                            @case(1)
                                @switch($adv->ad_platform)
                                    @case(0)
                                    ВКонтакте и сайты-партнёры
                                    @break

                                    @case(1)
                                    только ВКонтакте
                                    @break
                                @endswitch
                            @break

                            @case(9)
                            @switch($adv->ad_platform)
                                @case('all')
                                все площадки
                                @break

                                @case('desktop')
                                полная версия сайта
                                @break

                                @case('mobile')
                                мобильный сайт и приложения
                                @break
                            @endswitch
                            @break

                            @case(11)
                            @switch($adv->ad_platform)
                                @case('all')
                                все площадки
                                @break

                                @case('desktop')
                                полная версия сайта
                                @break

                                @case('mobile')
                                мобильный сайт и приложения
                                @break
                            @endswitch
                            @break

                            @default
                            не задано
                        @endswitch
                    </td>
                </tr>
                <tr>
                    <td>Ограничение показов:</td>
                    <td>
                        @if (isset($adv->impressions_limit))
                            Ограничить до {{$adv->pressions_limit}} показов на человека
                        @elseif (isset($adv->impressions_limited) && $adv->impressions_limited == 1)
                            Ограничить до 100 показов на человека
                        @else
                            не задано
                        @endif
                    </td>
                </tr>

                <tr>
                    <td>Целевая аудитория:</td>
                    <td>{{ $adv->target->count }}</td>
                </tr>

                <tr>
                    <td>Города</td>
                    <td>{{ $adv->target->cities->implode('title', ', ') }}</td>
                </tr>

                <tr>
                    <td>Демография:</td>
                    <td>
                        {{ isset($adv->target->sex) ? $adv->target->sex == 1 ? 'женщины' : 'мужчины' : 'все' }}
                        {{ isset($adv->target->age_from) || isset($adv->target->age_to) ? 'в возрасте' : '' }}
                        {{ isset($adv->target->age_from) ? ' от '.$adv->target->age_from : '' }}
                        {{ isset($adv->target->to) ? ' до '.$adv->target->to_from : '' }}

                    </td>
                </tr>
                @if(isset($adv->target->categoriesIn))
                <tr>
                    <td>Категории интересов:</td>
                    <td>
                        {{ $adv->target->categoriesIn->implode('name', ', ') }}
                    </td>
                </tr>
                @endif

                @if(isset($adv->target->categoriesNotIn))
                <tr>
                    <td>Исключить интересы и поведение:</td>
                    <td>
                        {{ $adv->target->categoriesNotIn->implode('name', ', ') ? $adv->target->categoriesNotIn->implode('name', ', ') : 'не задано'}}
                    </td>
                </tr>
                @endif

                <tr>
                    <td>Ссылка:</td>
                    <td>
                        <a href="{{ $adv->layout->link_url }}">{{ $adv->layout->link_url }}</a>
                    </td>
                </tr>
                <tr>
                    <td><a href="{{route('delete-ads', [$cabinet->account_id, $campaign->id, $adv->id])}}">В архив</a></td>
                    <td></td>
                </tr>
            </table>

            <form method="post" action="{{route('add-comment', [$cabinet->account_id, $campaign->id, $adv->id])}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <textarea name="comment" id="" cols="30" rows="10" maxlength="100" style="width: 100%; height: 25px;" >{{isset($adv->comment->comment) ? $adv->comment->comment : ''}}</textarea>
                <input type="submit" value="сохранить">
            </form>
        </div>
    @endforeach
</div>
