<div style="width: 1000px; margin-left: auto; margin-right: auto">

    @include('blocks.user')

    <div style="margin-bottom: 25px;">
        <a href="{{route('adv-cabinet')}}">Кабинеты </a>
    </div>
    <div>
        <h2>Рекламные кабинеты</h2>
    </div>

    @foreach ($cabinets as $cabinet)
        <div style="border-bottom: 1px solid rgba(223,223,223,1); padding: 10px 5px">
            <a href="{{route('get-adv-cabinet', $cabinet->account_id)}}"><span style="font-size: 25px; font-weight: bold">{{ $cabinet->account_name }}</span> id: {{ $cabinet->account_id }}</a>
        </div>
    @endforeach

</div>
