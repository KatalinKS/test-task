<div style="width: 1000px; margin-left: auto; margin-right: auto">

@include('blocks.user')

    <div style="margin-bottom: 25px;">
        <a href="{{route('adv-cabinet')}}">Кабинеты </a> \
        <a href="{{route('get-adv-cabinet', $cabinet->account_id)}}">{{$cabinet->account_name}}</a>
    </div>
    <div>
        <h2>Рекламные компании</h2>
    </div>
    @foreach ($companies as $company)
        <div style="border-bottom: 1px solid rgba(223,223,223,1); padding: 10px 5px">
            <a href="{{route('get-adv-company', [$cabinet->account_id, $company->id])}}"><span style="font-size: 25px; font-weight: bold">{{ $company->name }}</span> id: {{ $company->id }}</a>
        </div>
    @endforeach
</div>
