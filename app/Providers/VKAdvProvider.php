<?php

namespace App\Providers;

use App\Services\VKAdv\Clients\DefaultClient;
use App\Services\VKAdv\Interfaces\Builders\AdsBuilder;
use App\Services\VKAdv\Interfaces\Builders\AdsBuilderDirector;
use App\Services\VKAdv\Interfaces\Builders\TargetBuilder;
use App\Services\VKAdv\Interfaces\Builders\TargetBuilderDirector;
use App\Services\VKAdv\Interfaces\Client;
use App\Services\VKAdv\Interfaces\LocalRepository;
use App\Services\VKAdv\Interfaces\VKRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class VKAdvProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Interfaces\VKAdv::class, function($app) {

            return new \App\Services\VKAdv\VKAdv(
                app(Client::class),
                app(LocalRepository::class),
                app(VKRepository::class),
                app(AdsBuilderDirector::class),
                Auth::user()->getVKToken());
        });

        $this->app->singleton(Client::class, DefaultClient::class);

        $this->app->singleton(LocalRepository::class, \App\Services\VKAdv\Repositories\LocalRepository::class);
        $this->app->singleton(VKRepository::class, \App\Services\VKAdv\Repositories\VKRepository::class);

        $this->app->bind(AdsBuilder::class, \App\Services\VKAdv\Builders\AdsBuilder::class);
        $this->app->bind(AdsBuilderDirector::class, \App\Services\VKAdv\Builders\AdsBuilderDirector::class);
        $this->app->bind(TargetBuilder::class, \App\Services\VKAdv\Builders\TargetBuilder::class);
        $this->app->bind(TargetBuilderDirector::class, \App\Services\VKAdv\Builders\TargetBuilderDirector::class);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
