<?php

namespace App\Providers;

use App\Http\Controllers\OAuth\VKAuthController;
use App\Interfaces\OAuth;
use App\Services\OAuth\Clients\OAuthVKClient;
use App\Services\User\Interfaces\UserBuilder;
use Illuminate\Support\ServiceProvider;

class OAuthProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('oauth_vk_client', OAuthVKClient::class);

        $this->app
            ->when(VKAuthController::class)
            ->needs(OAuth::class)
            ->give(function($app) {
            return new \App\Services\OAuth\OAuth(
                new \App\Models\Auth\OAuth(),
                app('oauth_vk_client'),
                app(UserBuilder::class),
                'VK');
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
