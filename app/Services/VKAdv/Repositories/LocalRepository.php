<?php


namespace App\Services\VKAdv\Repositories;


use App\Models\Ads\Comment;
use App\Models\Ads\InterestCategory;
use App\Models\AdsCategory;
use App\Models\Ads\Account;
use App\Models\Ads\Campaign;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class LocalRepository implements \App\Services\VKAdv\Interfaces\LocalRepository
{
    private Model $accountModel;
    private Model $campaignModel;
    private Model $categoryModel;
    private Model $interestCategoryModel;
    private Model $commentModel;

    private ?Collection $categoryCollection = null;
    private ?Collection $interestCategoryCollection = null;

    public function __construct(Account $accountModel, Campaign $campaignModel, AdsCategory $categoryModel, InterestCategory $interestCategoryModel, Comment $commentModel)
    {
        $this->accountModel = $accountModel;
        $this->campaignModel = $campaignModel;
        $this->categoryModel = $categoryModel;
        $this->interestCategoryModel = $interestCategoryModel;
        $this->commentModel = $commentModel;
    }

    public function getAccount(int $accountId): ?object
    {
        return $this->accountModel
            ->where('account_id', '=', $accountId)
            ->first();
    }

    public function recordAccount(object $obj): void
    {
        $this->accountModel->create((array) $obj);
    }

    public function getCampaign(int $campaignId): ?object
    {
        return $this->campaignModel
            ->find($campaignId);
    }

    public function recordCampaign(object $obj): void
    {
        $this->campaignModel->create((array) $obj);
    }

    public function recordCategory(object $obj): void
    {
        $this->categoryModel->create(['id' => $obj->id, 'name' => $obj->name]);
    }

    public function getCategories(bool $reload = false): Collection
    {
        if ($this->categoryCollection == null || $reload) {
            $this->loadCategories();
        }

        return $this->categoryCollection;
    }

    public function getCategoriesById(array $ids):Collection
    {
        return $this->getCategories()->whereIn('id', $ids);
    }

    private function loadCategories(): void
    {
        $this->categoryCollection = $this->categoryModel->all();
    }

    public function recordInterestCategories(object $obg): void
    {
        $this->interestCategoryModel->create((array) $obg);
    }

    public function getInterestCategories(bool $reload = false): Collection
    {
        if ($this->interestCategoryCollection == null || $reload) {
            $this->loadInterestCategories();
        }

        return $this->interestCategoryCollection;
    }

    public function getInterestCategoriesById(array $ids): Collection
    {
        return $this->getInterestCategories()->whereIn('id', $ids);
    }

    private function loadInterestCategories(): void
    {
        $this->interestCategoryCollection = $this->interestCategoryModel->all();
    }

    public function getComments(array $adsIds): Collection
    {
        return $this->commentModel->whereIn('ads_id', $adsIds)->get();
    }
}
