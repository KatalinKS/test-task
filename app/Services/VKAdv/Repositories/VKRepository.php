<?php


namespace App\Services\VKAdv\Repositories;


use App\Services\VKAdv\Interfaces\Client;

class VKRepository implements \App\Services\VKAdv\Interfaces\VKRepository
{
    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getAccount(int $accountId, string $token): ?object
    {
        $accounts = $this->getAccounts($token);

        foreach($accounts as $account) {
            if($account->account_id == $accountId) {
                return $account;
            } else {
                throw new \Exception('Account not found');
            }
        }
    }

    public function getAccounts(string $token): array
    {
        return $this->client->getAccounts($token);
    }

    public function getCampaigns(int $accountId, string $token): array
    {
        return $this->client->getCampaigns($accountId, $token);
    }

    public function getCampaign(int $campaignId, int $accountId, string $token): object
    {
        $campaigns = $this->getCampaigns($accountId, $token);

        foreach ($campaigns as $campaign) {
            if($campaign->id == $campaignId) {
                return $campaign;
            } else {
                throw new \Exception('Campaign not found');
            }
        }
    }

    public function getCategories(): array
    {
        return $this->getCategories();
    }

    public function deleteAds(int $adsId, int $accountId, string $token): void
    {
        $this->client->deleteAds($adsId, $accountId, $token);
    }
}
