<?php


namespace App\Services\VKAdv;

use App\Services\VKAdv\Interfaces\Builders\AdsBuilderDirector;
use App\Services\VKAdv\Interfaces\Client;
use App\Services\VKAdv\Interfaces\LocalRepository;
use App\Services\VKAdv\Interfaces\VKRepository;

class VKAdv implements \App\Interfaces\VKAdv
{
    private Client $client;
    private AdsBuilderDirector $adsBuilderDirector;
    private string $token;

    private LocalRepository $localRepository;
    private VKRepository $vkRepository;

    public function __construct(Client $client, LocalRepository $repository, VKRepository $vkRepository, AdsBuilderDirector $adsBuilderDirector,  string $token)
    {
        $this->client = $client;
        $this->token = $token;

        $this->localRepository = $repository;
        $this->vkRepository = $vkRepository;
        $this->adsBuilderDirector = $adsBuilderDirector;
    }

    public function getAccounts(): array {
        return $this->vkRepository->getAccounts($this->token);
    }

    public function getAccount(int $accountId): ?object
    {
        $account = $this->localRepository->getAccount($accountId);

        if($account == null) {
            $account = $this->vkRepository->getAccount($accountId, $this->token);

            $this->localRepository->recordAccount($account);
        }
        return $account;
    }

    public function getCampaigns(int $accountId): array
    {
        return $this->vkRepository->getCampaigns($accountId, $this->token);
    }

    public function getCampaign(int $accountId, int $campaignId): object
    {
        $campaign = $this->localRepository->getCampaign($campaignId);

        if($campaign == null) {
            $campaign = $this->vkRepository->getCampaign($campaignId, $accountId, $this->token);

            $this->localRepository->recordCampaign($campaign);
        }
        return $campaign;
    }

    public function getAds(int $campaignId, int $accountId): \Illuminate\Support\Collection
    {
        $adsData = $this->client->getAdsData($campaignId, $accountId, $this->token);

        $adsCollection = collect($adsData[0]);
        $targetCollection = collect($adsData[1]);
        $citiesCollection = collect($adsData[2]);
        $countriesCollection = collect($adsData[3]);
        $layoutCollection = collect($adsData[4]);

        $ids = $adsCollection->pluck('id')->toArray();
        $comments = $this->localRepository->getComments($ids);

        $categories = $this->localRepository->getCategories();
        $interestCategory = $this->localRepository->getInterestCategories();

        $adsCollection = $this->adsBuilderDirector
            ->makeCollection(
                $adsCollection,
                $targetCollection,
                $layoutCollection,
                $comments,
                $citiesCollection,
                $countriesCollection,
                $categories,
                $interestCategory
            );

        return $adsCollection;
    }

    public function deleteAds(int $adsId, int $accountId): void
    {
        $this->vkRepository->deleteAds($adsId, $accountId, $this->token);
    }
}
