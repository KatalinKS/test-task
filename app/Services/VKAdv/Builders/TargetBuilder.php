<?php


namespace App\Services\VKAdv\Builders;

use Illuminate\Support\Collection;

class TargetBuilder implements \App\Services\VKAdv\Interfaces\Builders\TargetBuilder
{
    private ?object $instance = null;

    public function get(): object
    {
        return $this->instance;
    }

    public function fresh(): \App\Services\VKAdv\Interfaces\Builders\TargetBuilder
    {
        $this->instance = null;

        return $this;
    }

    public function setInterestCategories(Collection $categories): \App\Services\VKAdv\Interfaces\Builders\TargetBuilder
    {
        if (isset($this->instance->interest_categories_formula)) {
            $currentCategories = explode('&!', $this->instance->interest_categories_formula);

            preg_match_all("(\d+)", $currentCategories[0], $categroiesIn);
            preg_match_all("(\d+)", $currentCategories[1], $categroiesNotIn);

            $this->instance->categoriesIn = $categories->whereIn('id', $categroiesIn[0]);
            $this->instance->categoriesNotIn = $categories->whereIn('id', $categroiesNotIn[0]);
        }

        return $this;
    }

    public function setCities(Collection $cities): \App\Services\VKAdv\Interfaces\Builders\TargetBuilder
    {
        if (isset($this->instance->cities)) {
            $citiesId = explode(',', $this->instance->cities);
            $cities = $cities->whereIn('id', $citiesId);
            $this->instance->cities = $cities;
        }

        return $this;
    }

    public function setCountries(Collection $countries): \App\Services\VKAdv\Interfaces\Builders\TargetBuilder
    {
        if (isset($this->instance->countries)) {
            $countriesId = explode(',', $this->instance->countries);
            $countries = $countries->whereIn('id', $countriesId);
            $this->instance->cities = $countries;
        }

        return $this;
    }

    public function setTarget(object $obj): \App\Services\VKAdv\Interfaces\Builders\TargetBuilder
    {
        $this->instance = $obj;

        return $this;
    }
}
