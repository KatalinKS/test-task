<?php


namespace App\Services\VKAdv\Builders;


use App\Models\Ads\Comment;
use Illuminate\Support\Collection;

class AdsBuilder implements \App\Services\VKAdv\Interfaces\Builders\AdsBuilder
{
    private ?object $instance = null;

    public function fresh(): self
    {
        $this->instance = null;

        return $this;
    }

    public function setInstance(object $obj): \App\Services\VKAdv\Interfaces\Builders\AdsBuilder
    {
        $this->instance = $obj;

        return $this;
    }

    public function addComment(?Comment $comment): \App\Services\VKAdv\Interfaces\Builders\AdsBuilder
    {
        $this->instance->comment = $comment;

        return $this;
    }

    public function addTarget(object $obj): \App\Services\VKAdv\Interfaces\Builders\AdsBuilder
    {
        $this->instance->target = $obj;

        return $this;
    }

    public function addLayout(object $obj): \App\Services\VKAdv\Interfaces\Builders\AdsBuilder
    {
        $this->instance->layout = $obj;

        return $this;
    }

    public function addCategories(Collection $categories): \App\Services\VKAdv\Interfaces\Builders\AdsBuilder
    {
        $this->instance->categories = $categories->whereIn('id', [$this->instance->category1_id, $this->instance->category2_id]);

        return $this;
    }

    public function get(): object
    {
        return $this->instance;
    }

}
