<?php


namespace App\Services\VKAdv\Builders;


use App\Services\VKAdv\Interfaces\Builders\TargetBuilder;
use Illuminate\Support\Collection;

class TargetBuilderDirector implements \App\Services\VKAdv\Interfaces\Builders\TargetBuilderDirector
{
    private TargetBuilder $builder;

    /**
     * TargetBuilderDirector constructor.
     * @param TargetBuilder $builder
     */
    public function __construct(TargetBuilder $builder)
    {
        $this->builder = $builder;
    }

    public function make(object $target, Collection $countries, Collection $cities, Collection $interestCategories): object
    {
        return $this->builder
            ->fresh()
            ->setTarget($target)
            ->setCountries($countries)
            ->setCities($cities)
            ->setInterestCategories($interestCategories)
            ->get();
    }
}
