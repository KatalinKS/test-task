<?php


namespace App\Services\VKAdv\Builders;


use App\Models\Ads\Comment;
use App\Services\VKAdv\Interfaces\Builders\AdsBuilder;
use App\Services\VKAdv\Interfaces\Builders\TargetBuilderDirector;
use Illuminate\Support\Collection;

class AdsBuilderDirector implements \App\Services\VKAdv\Interfaces\Builders\AdsBuilderDirector
{
    private AdsBuilder $builder;
    private TargetBuilderDirector $targetBuilderDirector;

    /**
     * AdsBuilderDirector constructor.
     * @param AdsBuilder $builder
     * @param TargetBuilderDirector $targetBuilderDirector
     */
    public function __construct(AdsBuilder $builder, TargetBuilderDirector $targetBuilderDirector)
    {
        $this->builder = $builder;
        $this->targetBuilderDirector = $targetBuilderDirector;
    }

    public function makeCollection(
        Collection $adsCollection,
        Collection $adsTargets,
        Collection $adsLayout,
        Collection $comments,
        Collection $cities,
        Collection $countries,
        Collection $category,
        Collection $interestCategory
    ): Collection
    {
        $collection = new Collection();

        foreach ($adsCollection as $ads) {

            $target = $adsTargets->where('id', '=', $ads->id)->first();
            $layout = $adsLayout->where('id', '=', $ads->id)->first();
            $comment = $comments->where('ads_id', '=', $ads->id)->first();

            $ads = $this->make($ads,$target, $layout, $comment, $cities, $countries, $category, $interestCategory);

            $collection->add($ads);
        }

        return $collection;
    }

    public function make(
        object $ads,
        object $adsTarget,
        object $adsLayout,
        ?Comment $comment,
        Collection $cities,
        Collection $countries,
        Collection $category,
        Collection $interestCategory
    ): object
    {
        $adsTarget = $this->targetBuilderDirector->make($adsTarget, $countries, $cities, $interestCategory);

        return $this->builder
            ->fresh()
            ->setInstance($ads)
            ->addComment($comment)
            ->addTarget($adsTarget)
            ->addLayout($adsLayout)
            ->addCategories($category)
            ->get();
    }
}
