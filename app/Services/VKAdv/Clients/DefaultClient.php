<?php


namespace App\Services\VKAdv\Clients;


use Illuminate\Support\Facades\Http;

class DefaultClient implements \App\Services\VKAdv\Interfaces\Client
{
    private string $host = 'https://api.vk.com/method/';
    private string $apiVersion = '5.131';

    public function getAccounts(string $token): array
    {
        $response = Http::get($this->host.'ads.getAccounts', ['access_token' => $token, 'v' => $this->apiVersion]);

        return $response->object()->response;
    }

    public function getCampaigns(int $accountId, string $token): array
    {
        $queryParam = [
            'access_token' => $token,
            'account_id' => $accountId,
            'v' => $this->apiVersion
        ];

        $response = Http::get($this->host.'ads.getCampaigns', $queryParam);

        //TODO: попробовать закешировать кампании при первом же запросе
        sleep(1);
        return $response->object()->response;
    }

    public function getAdsData(int $campaignId, int $accountId, string $token): array
    {
        $isNeedWait = false;

        $tmpResponse = [];
        $codes = [
            '
            var ads = API.ads.getAds({"account_id":'.$accountId.'});
            var target = API.ads.getAdsTargeting({"account_id":'.$accountId.'});
            var cities = API.database.getCitiesById({\'city_ids\':target@.cities+\',\'+target@.cities_not});
            var countries = API.database.getCountriesById({\'country_ids\':target@.country});


            return [
                ads,
                target,
                cities,
                countries,
            ];
            ',
            '
            return [
                API.ads.getAdsLayout({"account_id":'.$accountId.'})
            ];
            '
        ];

        foreach ($codes as $code) {
            if($isNeedWait) {
                sleep(1);
            } else {
                $isNeedWait = true;
            }

            $queryParam = [
                'access_token' => $token,
                'campaign_ids' => json_encode(['id' => $campaignId]),
                'account_id' => $accountId,
                'include_deleted' => 0,
                'code' => $code,
                'v' => $this->apiVersion
            ];

            $response = Http::get($this->host.'execute', $queryParam);

            $tmpResponse = array_merge($tmpResponse, $response->object()->response);
        }

        return $tmpResponse;
    }

    private function getFloodStats(int $accountId, string $token)
    {
        sleep(1);
        $queryParam = [
            'access_token' => $token,
            'account_id' => $accountId,
            'v' => $this->apiVersion
        ];

        $response = Http::get($this->host.'execute', $queryParam);

        var_dump($response->object()->response);
    }

    public function deleteAds(int $adsId, int $accountId, string $token): void
    {
        $queryParam = [
            'access_token' => $token,
            'account_id' => $accountId,
            'ids' => json_encode(['id' => $adsId]),
            'v' => $this->apiVersion
        ];

        $response = Http::get($this->host.'ads.deleteAds', $queryParam);
    }
}
