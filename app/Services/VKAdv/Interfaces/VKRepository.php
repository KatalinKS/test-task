<?php


namespace App\Services\VKAdv\Interfaces;


interface VKRepository
{
    public function getAccount(int $accountId, string $token): ?object;

    public function getAccounts(string $token): array;

    public function getCampaigns(int $accountId, string $token): array;

    public function getCampaign(int $campaignId, int $accountId, string $token): object;

    public function getCategories(): array;

    public function deleteAds(int $adsId, int $accountId, string $token): void;
}
