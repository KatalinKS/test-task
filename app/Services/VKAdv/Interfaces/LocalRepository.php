<?php


namespace App\Services\VKAdv\Interfaces;


use Illuminate\Database\Eloquent\Collection;

interface LocalRepository
{
    public function getAccount(int $accountId): ?object;

    public function recordAccount(object $obj): void;

    public function getCampaign(int $campaignId): ?object;

    public function recordCampaign(object $obj): void;

    public function recordCategory(object $obj): void;

    public function getCategories(bool $reload = false): Collection;

    public function getCategoriesById(array $ids):Collection;

    public function getInterestCategories(bool $reload = false): Collection;

    public function getInterestCategoriesById(array $ids):Collection;

    public function recordInterestCategories(object $obg): void;

    public function getComments(array $adsIds):Collection;

}
