<?php


namespace App\Services\VKAdv\Interfaces;


interface Client
{
    public function getAccounts(string $token): array;

    public function getCampaigns(int $accountId, string $token): array;

    public function getAdsData(int $campaignId, int $accountId, string $token): array ;

    public function deleteAds(int $adsId, int $accountId, string $token): void;
}
