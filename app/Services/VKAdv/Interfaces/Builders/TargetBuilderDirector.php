<?php


namespace App\Services\VKAdv\Interfaces\Builders;

use Illuminate\Support\Collection;

interface TargetBuilderDirector
{
    public function make(object $target, Collection $countries, Collection $cities, Collection $interestCategories): object;
}
