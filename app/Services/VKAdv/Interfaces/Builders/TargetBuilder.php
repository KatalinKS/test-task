<?php


namespace App\Services\VKAdv\Interfaces\Builders;

use Illuminate\Database\Eloquent\Collection;

interface TargetBuilder
{
    public function get(): object;
    public function fresh(): self;
    public function setInterestCategories(Collection $categories): self;
    public function setCities(Collection $cities): self;
    public function setCountries(Collection $countries): self;
    public function setTarget(object $obj):self;
}
