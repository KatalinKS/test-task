<?php


namespace App\Services\VKAdv\Interfaces\Builders;


use App\Models\Ads\Comment;
use Illuminate\Support\Collection;

interface AdsBuilder
{
    public function fresh():self;
    public function setInstance(object $obj): self;
    public function addComment(?Comment $comment):self;
    public function addTarget(object $obj):self;
    public function addLayout(object $obj):self;
    public function addCategories(Collection $categories):self;
    public function get():object;
}
