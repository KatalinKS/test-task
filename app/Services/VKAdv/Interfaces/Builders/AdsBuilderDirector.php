<?php


namespace App\Services\VKAdv\Interfaces\Builders;




use Illuminate\Support\Collection;

interface AdsBuilderDirector
{
    public function makeCollection(
        Collection $ads,
        Collection $adsTargets,
        Collection $adsLayout,
        Collection $comments,
        Collection $cities,
        Collection $countries,
        Collection $category,
        Collection $interestCategory
    ): Collection;
}
