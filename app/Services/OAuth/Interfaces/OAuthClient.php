<?php


namespace App\Services\OAuth\Interfaces;


interface OAuthClient
{
    public function getLoginURL(): string;

    public function getAccessToken(string $code): array;

    public function getUserData(string $accessToken): object;
}
