<?php


namespace App\Services\OAuth;


use App\Models\User;
use \App\Models\Auth\OAuth as Model;
use App\Services\OAuth\Interfaces\OAuthClient;
use App\Services\User\Interfaces\UserBuilder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class OAuth implements \App\Interfaces\OAuth
{
    private OAuthClient $client;
    private Model $model;
    private string $oAuthSystem;
    private UserBuilder $userBuilder;

    public function __construct(Model $model, OAuthClient $client, UserBuilder $userBuilder, string $oAuthSystem)
    {
        $this->model = $model;
        $this->oAuthSystem = $oAuthSystem;
        $this->client = $client;
        $this->userBuilder = $userBuilder;

    }


    public function findUser(int $oAuthUserId): ?User
    {
        $oAuth = $this->model
            ->where('oauth_user_id', '=', $oAuthUserId)
            ->where('oauth_system', '=', $this->oAuthSystem)
            ->first();
        if($oAuth != null) {
            return $oAuth->user;
        } else {
            return null;
        }
    }

    public function getLoginURL(): string
    {
        return $this->client->getLoginURL();
    }

    public function getAccessToken(string $code): array
    {
        return $this->client->getAccessToken($code);
    }

    public function authorize(array $authData): void
    {
        $user = $this->findUser($authData['user_id']);

        if($user == null) {
            $userData = $this->client->getUserData($authData['access_token']);

            $user = $this->userBuilder->create((array) $userData);

            $user->oauth()->create($this->prepareOAuthData($authData));
        }

        Auth::login($user);
    }

    private function prepareOAuthData(array $data): array
    {
        $now = new Carbon();

        return [
            'oauth_system' => $this->oAuthSystem,
            'oauth_user_id' => $data['user_id'],
            'access_token' => $data['access_token'],
            'expires_in' => $now->addSeconds($data['expires_in']),

        ];
    }
}
