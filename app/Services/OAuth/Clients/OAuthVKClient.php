<?php


namespace App\Services\OAuth\Clients;


use App\Services\OAuth\Interfaces\OAuthClient;
use Illuminate\Support\Facades\Http;
use VK\OAuth\VKOAuth;
use VK\OAuth\VKOAuthDisplay;
use VK\OAuth\VKOAuthResponseType;

class OAuthVKClient implements OAuthClient
{
    private VKOAuth $driverOAuth;

    private string $callBackURL;
    private int $clientId;
    private string $clientSecrete;
    private string $host = 'https://api.vk.com/method/';

    private string $apiVersion = '5.131';


    /**
     * OAuthVKClient constructor.
     * @param VKOAuth $driverOAuth
     */
    public function __construct(VKOAuth $driverOAuth)
    {
        $this->driverOAuth = $driverOAuth;

        $this->setConfig();
    }

    private function setConfig(): void
    {
        $this->callBackURL = config('o_auth.url_success_auth');
        $this->clientId = config('o_auth.client_id');
        $this->clientSecrete = config('o_auth.client_secrete');
    }

    public function getLoginURL(): string
    {
        return $this->driverOAuth->getAuthorizeUrl(
            VKOAuthResponseType::CODE,
            $this->clientId,
            $this->callBackURL,
            VKOAuthDisplay::PAGE,
            [32768]
        );
    }

    public function getAccessToken(string $code): array
    {
        return $this->driverOAuth->getAccessToken(
            $this->clientId,
            $this->clientSecrete,
            $this->callBackURL,
            $code
        );
    }

    public function getUserData(string $accessToken): object
    {
        $query = [
            'v' => $this->apiVersion,
            'access_token' => $accessToken,
            'fields' => 'photo_100',
        ];

        $response = Http::get($this->host.'users.get', $query);

        return $response->object()->response[0];
    }
}
