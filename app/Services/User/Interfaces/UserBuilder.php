<?php


namespace App\Services\User\Interfaces;


use App\Models\User;

interface UserBuilder
{
    public function create(array $userData): User;
}
