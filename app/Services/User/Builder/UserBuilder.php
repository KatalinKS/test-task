<?php


namespace App\Services\User\Builder;


use App\Models\User;

class UserBuilder implements \App\Services\User\Interfaces\UserBuilder
{
    private User $instance;

    private function getInstance(): User
    {
        return new User();
    }

    private function fresh(): self
    {
        $this->instance = $this->getInstance();

        return $this;
    }

    public function create(array $userData): User
    {
        $this->fresh();

        $this->instance->fill($userData)->save();

        return $this->instance;
    }
}
