<?php

namespace App\Models;

use App\Models\Auth\OAuth;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'photo_100',
    ];
    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function oauth(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(OAuth::class);
    }

    public function getVKToken(): ?string
    {
        $vkOAuth = $this->oauth->where('oauth_system', '=', 'VK')->first();

        if ($vkOAuth != null) {
            return $vkOAuth->access_token;
        } else {
            return null;
        }
    }
}
