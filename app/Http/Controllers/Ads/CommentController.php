<?php

namespace App\Http\Controllers\Ads;

use App\Http\Controllers\Controller;
use App\Interfaces\VKAdv;
use App\Models\Ads\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function add(Request $request, Comment $comment, int $accountId, int $campaignId, int $adsId)
    {
        $comment->updateOrCreate(
            [
                'ads_id' => $adsId,
            ],
            [
                'comment' => $request->get('comment')
            ]
        );
        return redirect(route('get-adv-company', [$accountId, $campaignId]));
    }
}
