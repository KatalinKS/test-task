<?php

namespace App\Http\Controllers\Ads;

use App\Http\Controllers\Controller;
use App\Interfaces\VKAdv;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class AccountsController extends Controller
{
    public function index(VKAdv $vkAdv)
    {
        $cabinets = $vkAdv->getAccounts();

        return view('cabinet.index', ['cabinets' => $cabinets]);
    }

    public function get(VKAdv $vkAdv, int $id)
    {
        $cabinet = $vkAdv->getAccount($id);
        $companies = $vkAdv->getCampaigns($id);

        return view('cabinet.cabinet', ['cabinet' => $cabinet, 'companies' => $companies]);
    }
}
