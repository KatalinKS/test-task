<?php

namespace App\Http\Controllers\Ads;

use App\Http\Controllers\Controller;
use App\Interfaces\VKAdv;
use Illuminate\Http\Request;

class CampaignController extends Controller
{
    public function index(VKAdv $vkAdv, int $accountId, int $campaignId)
    {
        $cabinet = $vkAdv->getAccount($accountId);
        $campaign = $vkAdv->getCampaign($accountId, $campaignId);

        $advs = $vkAdv->getAds($campaignId, $accountId);

        return view('campaign.index', ['cabinet' => $cabinet, 'campaign' => $campaign, 'advs' => $advs]);
    }
}
