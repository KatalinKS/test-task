<?php

namespace App\Http\Controllers\Ads;

use App\Http\Controllers\Controller;
use App\Interfaces\VKAdv;
use Illuminate\Http\Request;

class AdsController extends Controller
{
    public function delete(VKAdv $vkAds, int $accountId, int $campaignId, int $adsId)
    {
        $vkAds->deleteAds($adsId, $accountId);

        return redirect(route('get-adv-company', [$accountId, $campaignId]));
    }
}
