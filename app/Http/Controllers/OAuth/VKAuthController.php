<?php

namespace App\Http\Controllers\OAuth;

use App\Http\Controllers\Controller;
use App\Interfaces\OAuth;
use Illuminate\Http\Request;
use VK\OAuth\VKOAuth;

class VKAuthController extends Controller
{
    private OAuth $facadeOAuth;

    public function __construct(OAuth $facadeOAuth)
    {
        $this->facadeOAuth = $facadeOAuth;
    }

    public function goToAuthorizeUrl()
    {
        $url = $this->facadeOAuth->getLoginURL();

        return redirect($url);
    }

    public function success(Request $request, VKOAuth $auth)
    {
        $authData = $this->facadeOAuth->getAccessToken($request->get('code'));

        $this->facadeOAuth->authorize($authData);

        return redirect(route('adv-cabinet'));
    }
}
