<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function loginForm()
    {
        return view('login');
    }

    public function logout()
    {
        Auth::logout();

        return redirect(route('login-form'));
    }
}
