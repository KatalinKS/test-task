<?php


namespace App\Interfaces;


use App\Models\User;

interface OAuth
{
    public function getLoginURL(): string;

    public function getAccessToken(string $code):array;

    public function findUser(int $oAuthUserId): ?User;

    public function authorize(array $authData): void;
}
