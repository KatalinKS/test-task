<?php


namespace App\Interfaces;


interface VKAdv
{
    public function getAccounts(): array;
    public function getCampaigns(int $accountId): array;
    public function getAccount(int $accountId): ?object;
    public function getCampaign(int $accountId, int $campaignId): object;
    public function getAds(int $campaignId, int $accountId): \Illuminate\Support\Collection;
    public function deleteAds(int $adsId, int $accountId):void;
}
