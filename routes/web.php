<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function() {
    return redirect(route('adv-cabinet'));
});

Route::get('login', [\App\Http\Controllers\AuthController::class, 'loginForm'])->name('login-form');
Route::get('logout', [\App\Http\Controllers\AuthController::class, 'logout'])->name('logout');
Route::get('OAuth/vk/login', [\App\Http\Controllers\OAuth\VKAuthController::class, 'goToAuthorizeUrl'])->name('vk-login');
Route::get('OAuth/vk/success', [\App\Http\Controllers\OAuth\VKAuthController::class, 'success'])->name('vk-success');

Route::middleware(\App\Http\Middleware\Authenticate::class)->prefix('ads-cabinets')->group(function() {
    Route::get('/', [\App\Http\Controllers\Ads\AccountsController::class, 'index'])->name('adv-cabinet');
    Route::get('/{id}', [\App\Http\Controllers\Ads\AccountsController::class, 'get'])->name('get-adv-cabinet');

    Route::prefix('/{accountId}/company')->group(function() {
        Route::get('/{campaignId}', [\App\Http\Controllers\Ads\CampaignController::class, 'index'])->name('get-adv-company');

        Route::prefix('{campaignId}/ads')->group(function() {
            //TODO: тут должен быть делит а не гет
            Route::get('/{adsId}', [\App\Http\Controllers\Ads\AdsController::class, 'delete'])->name('delete-ads');

            Route::prefix('/{adsId}/comment')->group(function() {
                Route::post('/', [\App\Http\Controllers\Ads\CommentController::class, 'add'])->name('add-comment');
            });
        });
    });

});
